<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190605134258 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE special_consumption (id INT AUTO_INCREMENT NOT NULL, user INT DEFAULT NULL, dateCreated DATETIME NOT NULL, dateConsumption DATETIME NOT NULL, description VARCHAR(255) NOT NULL, amount INT NOT NULL, total_price NUMERIC(5, 2) NOT NULL, active TINYINT(1) NOT NULL, INDEX IDX_E77AFE518D93D649 (user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction_history (id INT AUTO_INCREMENT NOT NULL, user INT DEFAULT NULL, date DATETIME NOT NULL, amount NUMERIC(5, 2) NOT NULL, balance NUMERIC(5, 2) NOT NULL, INDEX IDX_51104CA98D93D649 (user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE special_consumption ADD CONSTRAINT FK_E77AFE518D93D649 FOREIGN KEY (user) REFERENCES user (id)');
        $this->addSql('ALTER TABLE transaction_history ADD CONSTRAINT FK_51104CA98D93D649 FOREIGN KEY (user) REFERENCES user (id)');

        $this->addSql('ALTER TABLE price_category CHANGE price price NUMERIC(5, 2) NOT NULL');
        $this->addSql('ALTER TABLE payment CHANGE amount amount NUMERIC(5, 2) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE special_consumption');
        $this->addSql('DROP TABLE transaction_history');
        $this->addSql('ALTER TABLE payment CHANGE amount amount INT NOT NULL');
        $this->addSql('ALTER TABLE permission CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE name name VARCHAR(128) NOT NULL COLLATE utf8_unicode_ci, CHANGE description description VARCHAR(1024) NOT NULL COLLATE utf8_unicode_ci, CHANGE date_created date_created DATETIME NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX name_idx ON permission (name)');
        $this->addSql('ALTER TABLE price_category CHANGE price price VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE role CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE name name VARCHAR(128) NOT NULL COLLATE utf8_unicode_ci, CHANGE description description VARCHAR(1024) NOT NULL COLLATE utf8_unicode_ci, CHANGE date_created date_created DATETIME NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX name_idx ON role (name)');
        $this->addSql('ALTER TABLE role_hierarchy DROP FOREIGN KEY FK_AB8EFB72B4B76AB7');
        $this->addSql('ALTER TABLE role_hierarchy DROP FOREIGN KEY FK_AB8EFB72A44B56EA');
        $this->addSql('ALTER TABLE role_hierarchy DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE role_hierarchy ADD id INT AUTO_INCREMENT NOT NULL, CHANGE child_role_id child_role_id INT NOT NULL, CHANGE parent_role_id parent_role_id INT NOT NULL');
        $this->addSql('ALTER TABLE role_hierarchy ADD CONSTRAINT role_role_child_role_id_fk FOREIGN KEY (child_role_id) REFERENCES role (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_hierarchy ADD CONSTRAINT role_role_parent_role_id_fk FOREIGN KEY (parent_role_id) REFERENCES role (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_hierarchy ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE role_permission DROP FOREIGN KEY FK_6F7DF886D60322AC');
        $this->addSql('ALTER TABLE role_permission DROP FOREIGN KEY FK_6F7DF886FED90CCA');
        $this->addSql('ALTER TABLE role_permission DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE role_permission ADD id INT AUTO_INCREMENT NOT NULL, CHANGE role_id role_id INT NOT NULL, CHANGE permission_id permission_id INT NOT NULL');
        $this->addSql('ALTER TABLE role_permission ADD CONSTRAINT role_permission_permission_id_fk FOREIGN KEY (permission_id) REFERENCES permission (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_permission ADD CONSTRAINT role_permission_role_id_fk FOREIGN KEY (role_id) REFERENCES role (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_permission ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE user CHANGE email email VARCHAR(128) NOT NULL COLLATE utf8_unicode_ci, CHANGE full_name full_name VARCHAR(512) NOT NULL COLLATE utf8_unicode_ci, CHANGE password password VARCHAR(256) NOT NULL COLLATE utf8_unicode_ci, CHANGE status status INT NOT NULL, CHANGE date_created date_created DATETIME NOT NULL, CHANGE pwd_reset_token pwd_reset_token VARCHAR(256) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE pwd_reset_token_creation_date pwd_reset_token_creation_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user_role DROP FOREIGN KEY FK_2DE8C6A3A76ED395');
        $this->addSql('ALTER TABLE user_role DROP FOREIGN KEY FK_2DE8C6A3D60322AC');
        $this->addSql('ALTER TABLE user_role DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE user_role ADD id INT AUTO_INCREMENT NOT NULL, CHANGE role_id role_id INT NOT NULL');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT user_role_role_id_fk FOREIGN KEY (role_id) REFERENCES role (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT user_role_user_id_fk FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_role ADD PRIMARY KEY (id)');
    }
}
