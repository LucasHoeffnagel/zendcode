FROM php:7.1-apache

RUN apt-get update \
 && apt-get install -y git zlib1g-dev \
                        libfreetype6-dev \
                        libjpeg62-turbo-dev \
                        libpng-dev \
                        libicu-dev \
 && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
 && docker-php-ext-configure intl \
 && docker-php-ext-install gd intl zip pdo pdo_mysql \
 && a2enmod rewrite \
 && sed -i 's!/var/www/html!/var/www/public!g' /etc/apache2/sites-available/000-default.conf \
 && mv /var/www/html /var/www/public \
 && curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www
