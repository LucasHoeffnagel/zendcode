<?php

namespace Product\Service\Factory;

use Interop\Container\ContainerInterface;
use Product\Service\PriceCategoryService;
use Product\Service\ProductService;

class PriceCategoryServiceFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        // Instantiate the controller and inject dependencies
        return new PriceCategoryService($entityManager);
    }
}