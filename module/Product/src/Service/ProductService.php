<?php

namespace Product\Service;


use Product\Entity\PriceCategory;
use Product\Entity\Product;

class ProductService
{
    private $entityManager;

    /**
     * ProductService constructor.
     * @param $entityManager
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Edit a product
     *
     * @param Product $product
     * @param $data
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editProduct(Product $product, $data)
    {
        $priceCategory = $this->entityManager->getRepository(PriceCategory::class)->find($data['priceCategory']);

        $product->setName($data['productName']);
        $product->setPriceCategory($priceCategory);
        $product->setStock($data['productStock']);

        $this->entityManager->merge($product);

        $this->entityManager->flush();

        return true;
    }
}