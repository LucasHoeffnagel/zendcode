<?php

namespace Product\Service;


use Product\Entity\PriceCategory;
use Product\Entity\Product;

class PriceCategoryService
{
    private $entityManager;

    /**
     * ProductService constructor.
     * @param $entityManager
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Edit a price category
     *
     * @param PriceCategory $priceCategory
     * @param $data
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editPriceCategory(PriceCategory $priceCategory, $data)
    {

        $priceCategory->setName($data['categoryName']);
        $priceCategory->setPrice($data['categoryPrice']);

        $this->entityManager->merge($priceCategory);

        $this->entityManager->flush();

        return true;
    }
}