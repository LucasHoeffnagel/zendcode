<?php
namespace Product\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a registered user.
 * @ORM\Entity(repositoryClass="\Product\Repository\ProductRepository")
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name")
     */
    protected $name;

    /**
     * @ORM\Column(name="stock", type="integer")
     */
    protected $stock;

   /**
     * Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="PriceCategory", inversedBy="product")
     * @ORM\JoinColumn(name="price_category", referencedColumnName="id")
     */
    protected $priceCategory;

    /**
     * @ORM\OneToMany(targetEntity="Consumption\Entity\Consumption",mappedBy="product")
     */
    protected $consumption;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock($stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getPriceCategory()
    {
        return $this->priceCategory;
    }

    /**
     * @param mixed $priceCategory
     */
    public function setPriceCategory($priceCategory): void
    {
        $this->priceCategory = $priceCategory;
    }
}