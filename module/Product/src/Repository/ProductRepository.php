<?php
namespace Product\Repository;


use Doctrine\ORM\EntityRepository;
use Product\Entity\Product;

class ProductRepository extends EntityRepository
{
    /**
     * Find all products and their price category
     * @return \Doctrine\ORM\Query
     */
    public function findAllProducts() {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();
        $queryBuilder->select('p.id, p.name, pc.name AS category_name, pc.price, p.stock')
            ->from(Product::class, 'p')
            ->leftJoin('p.priceCategory', 'pc');

        return $queryBuilder->getQuery();
    }
}