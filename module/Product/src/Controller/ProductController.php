<?php

namespace Product\Controller;

use Product\Entity\PriceCategory;
use Product\Entity\Product;
use Product\Form\ProductForm;
use Product\Service\ProductService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ProductController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;
    /**
     * @var ProductService
     */
    private $productService;

    /**
     * IndexController constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param ProductService $productService
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager, ProductService $productService)
    {
        $this->entityManager = $entityManager;
        $this->productService = $productService;
    }

    /**
     * List all products and add new ones
     *
     * @return \Zend\Http\Response|ViewModel
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function indexAction()
    {
        $products = $this->entityManager->getRepository(Product::class)
            ->findAllProducts()->getResult();

        $form = new ProductForm($this->entityManager);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);
            if (!$form->isValid()) {
                $this->flashMessenger()->addErrorMessage($form->getMessages());

                return $this->redirect()->toRoute('product', ['action' => 'index']);
            } else {
                $data = $form->getData();

                $product = new Product();

                $priceCategory = $this->entityManager->getRepository(PriceCategory::class)->find($data['priceCategory']);

                $product->setName($data['productName']);
                $product->setPriceCategory($priceCategory);
                $product->setStock($data['productStock']);

                $this->entityManager->persist($product);

                $this->entityManager->flush();

                $this->flashMessenger()->addSuccessMessage('Product is toegevoegd.');

                // Redirect to "view" page
                return $this->redirect()->toRoute('product', ['action' => 'index']);
            }
        }

        return new ViewModel([
            'products' => $products,
            'form' => $form
        ]);
    }

    /**
     * Edit a product
     *
     * @return \Zend\Http\Response|ViewModel
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);

        /** @var Product $product */
        $product = $this->entityManager->getRepository(Product::class)->find($id);
        $products = $this->entityManager->getRepository(Product::class)
            ->findAllProducts()->getResult();

        $form = new ProductForm($this->entityManager);

        $form->setData([
            'productName' => $product->getName(),
            'priceCategory' => $product->getPriceCategory(),
            'productStock' => $product->getStock()

        ]);

        if ($this->getRequest()->isPost()) {

            $data = $this->params()->fromPost();
            $form->setData($data);

            if (!$form->isValid()) {
                $this->flashMessenger()->addErrorMessage($form->getMessages());

                return $this->redirect()->toRoute('product', ['action' => 'index']);
            } else {
                $data = $form->getData();

                if ($this->productService->editProduct($product, $data)) {
                    $this->flashMessenger()->addSuccessMessage('Product is aangepast.');

                    // Redirect to "view" page
                    return $this->redirect()->toRoute('product', ['action' => 'index']);
                }
            }
        }

        return new ViewModel([
            'product' => $product,
            'form' => $form,
            'products' => $products
        ]);
    }

    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);

        $product = $this->entityManager->getRepository(Product::class)->find($id);

        $this->entityManager->remove($product);
        $this->entityManager->flush();

        $this->flashMessenger()->addSuccessMessage('Product is verwijderd.');

        // Redirect to "view" page
        return $this->redirect()->toRoute('product', ['action' => 'index']);

    }

}