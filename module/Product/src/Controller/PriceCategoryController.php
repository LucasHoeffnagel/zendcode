<?php

namespace Product\Controller;


use Product\Entity\PriceCategory;
use Product\Form\PriceCategoryForm;
use Product\Service\PriceCategoryService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class PriceCategoryController extends AbstractActionController
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;
    /**
     * @var PriceCategoryService
     */
    private $priceCategoryService;

    /**
     * PriceCategoryController constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param PriceCategoryService $priceCategoryService
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager, PriceCategoryService $priceCategoryService)
    {
        $this->entityManager = $entityManager;
        $this->priceCategoryService = $priceCategoryService;
    }

    /**
     * Create a price category
     *
     * @return \Zend\Http\Response|ViewModel
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function indexAction()
    {
        $priceCategories = $this->entityManager->getRepository(PriceCategory::class)
            ->findAll();

        $form = new PriceCategoryForm($this->entityManager);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);
            if (!$form->isValid()) {
                $this->flashMessenger()->addErrorMessage($form->getMessages());

                return $this->redirect()->toRoute('prijscategorie', ['action' => 'index']);
            } else {
                $data = $form->getData();

                $priceCategory = new PriceCategory();

                $priceCategory->setName($data['categoryName']);
                $priceCategory->setPrice($data['categoryPrice']);

                $this->entityManager->persist($priceCategory);

                $this->entityManager->flush();

                $this->flashMessenger()->addSuccessMessage('Categorie is toegevoegd.');

                // Redirect to "view" page
                return $this->redirect()->toRoute('prijscategorie', ['action' => 'index']);
            }
        }

        return new ViewModel([
            'priceCategories' => $priceCategories,
            'form' => $form
        ]);
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);

        /** @var PriceCategory $priceCategory */
        $priceCategory = $this->entityManager->getRepository(PriceCategory::class)->find($id);
        $priceCategories = $this->entityManager->getRepository(PriceCategory::class)
            ->findAll();

        $form = new PriceCategoryForm($this->entityManager);

        $form->setData([
            'categoryName' => $priceCategory->getName(),
            'categoryPrice' => $priceCategory->getPrice()
        ]);

        if ($this->getRequest()->isPost()) {

            $data = $this->params()->fromPost();
            $form->setData($data);

            if (!$form->isValid()) {
                $this->flashMessenger()->addErrorMessage($form->getMessages());

                return $this->redirect()->toRoute('prijscategorie', ['action' => 'index']);
            } else {
                $data = $form->getData();

                if ($this->priceCategoryService->editPriceCategory($priceCategory, $data)) {
                    $this->flashMessenger()->addSuccessMessage('Categorie is aangepast.');

                    // Redirect to "view" page
                    return $this->redirect()->toRoute('prijscategorie', ['action' => 'index']);
                }
            }
        }

        return new ViewModel([
            'priceCategory' => $priceCategory,
            'form' => $form,
            'priceCategories' => $priceCategories
        ]);
    }

    /**
     * Remove a price category
     *
     * @return \Zend\Http\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);

        $priceCategory = $this->entityManager->getRepository(PriceCategory::class)->find($id);

        $this->entityManager->remove($priceCategory);
        $this->entityManager->flush();

        $this->flashMessenger()->addSuccessMessage('Categorie is verwijderd.');

        // Redirect to "view" page
        return $this->redirect()->toRoute('prijscategorie', ['action' => 'index']);

    }
}