<?php
namespace Product\Controller\Factory;

use Interop\Container\ContainerInterface;
use Product\Controller\ProductController;
use Product\Service\ProductService;
use Zend\ServiceManager\Factory\FactoryInterface;

class ProductControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $productService = $container->get(ProductService::class);

        // Instantiate the controller and inject dependencies
        return new ProductController($entityManager, $productService);
    }
}