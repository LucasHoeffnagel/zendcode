<?php

namespace Product\Form;


use Doctrine\ORM\EntityManager;
use Product\Entity\PriceCategory;
use Zend\Form\Element\Csrf;
use Zend\Form\Form;

class ProductForm extends Form
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        // Define form name
        parent::__construct('product-form');

        $this->entityManager = $entityManager;

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        // Add form elements
        $this->addElements();
        $this->addInputFilter();
    }

    private function addElements()
    {
        $this->add([
            'type' => Csrf::class,
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600,
                ],
            ],
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'productName',
            'attributes' => [
                'id' => 'productName',
                'placeholder' => 'Product naam',
                'class' => 'form-control input-sm',
                'required' => true
            ]
        ]);

        $this->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'priceCategory',
            'attributes' => [
                'id' => 'priceCategory',
                'placeholder' => 'Categorie',
                'class' => 'form-control input-sm'
            ],
            'options' => [
                'object_manager' => $this->entityManager,
                'target_class' => PriceCategory::class,
                'label_generator' => function ($targetEntity) {
                    return $targetEntity->getName() . ' - €' . $targetEntity->getPrice();
                },
                'find_method' => [
                    'name' => 'findBy',
                    'params' => [
                        'criteria' => [],
                        'orderBy' => ['name' => 'asc'],
                    ]
                ]
            ]
        ]);

        $this->add([
            'type' => 'number',
            'name' => 'productStock',
            'attributes' => [
                'id' => 'productStock',
                'placeholder' => 'Voorraad',
                'class' => 'form-control input-sm',
                'min' => 0,
                'max' => 1337,
                'required' => true
            ]
        ]);

        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Opslaan',
                'class' => 'btn btn-primary pull-right'
            ],
        ]);
    }

    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name' => 'productName',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ]
        ]);

        $inputFilter->add([
            'name' => 'productStock',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ]
        ]);

    }
}