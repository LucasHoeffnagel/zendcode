<?php

namespace Product\Form;


use Doctrine\ORM\EntityManager;
use Zend\Form\Element\Csrf;
use Zend\Form\Form;

class PriceCategoryForm extends Form
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        // Define form name
        parent::__construct('pricecategory-form');

        $this->entityManager = $entityManager;

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        // Add form elements
        $this->addElements();
        $this->addInputFilter();
    }

    private function addElements()
    {
        $this->add([
            'type' => Csrf::class,
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600,
                ],
            ],
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'categoryName',
            'attributes' => [
                'id' => 'categoryName',
                'placeholder' => 'Categorie naam',
                'class' => 'form-control input-sm',
                'required' => true
            ]
        ]);

        $this->add([
            'type' => 'number',
            'name' => 'categoryPrice',
            'attributes' => [
                'id' => 'categoryPrice',
                'placeholder' => 'Prijs',
                'class' => 'form-control input-sm',
                'min' => 0,
                'max' => 1337,
                'step' => 0.01,
                'required' => true
            ]
        ]);

        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Opslaan',
                'class' => 'btn btn-primary pull-right'
            ],
        ]);
    }

    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name' => 'categoryName',
            'required' => true
        ]);

        $inputFilter->add([
            'name' => 'categoryPrice',
            'required' => true
        ]);

    }
}