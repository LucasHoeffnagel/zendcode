<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * This view helper class displays a menu bar.
 */
class Menu extends AbstractHelper 
{
    /**
     * Menu items array.
     * @var array 
     */
    protected $items = [];
    
    /**
     * Active item's ID.
     * @var string  
     */
    protected $activeItemId = '';
    
    /**
     * Constructor.
     * @param array $items Menu items.
     */
    public function __construct($items=[]) 
    {
        $this->items = $items;
    }
    
    /**
     * Sets menu items.
     * @param array $items Menu items.
     */
    public function setItems($items) 
    {
        $this->items = $items;
    }
    
    /**
     * Sets ID of the active items.
     * @param string $activeItemId
     */
    public function setActiveItemId($activeItemId) 
    {
        $this->activeItemId = $activeItemId;
    }
    
    /**
     * Renders the menu.
     * @return string HTML code of the menu.
     */
    public function render() 
    {

        $menuItems = [];

        if (count($this->items)==0)
            return ''; // Do nothing if there are no items.

        
        // Render items
        foreach ($this->items as $item) {
            if(!isset($item['float']) || $item['float']=='left') {
                $menuItems[] = $item;
            }

            if(!isset($item['float']) || $item['float']=='right') {

            }

        }
        
        return $menuItems;
        
    }

    /**
     * @return string
     */
    public function getActiveItemId(): string
    {
        return $this->activeItemId;
    }
}
