<?php

namespace Application\Service;

/**
 * This service is responsible for determining which items should be in the main menu.
 * The items may be different depending on whether the user is authenticated or not.
 */
class NavManager
{
    /**
     * Auth service.
     * @var Zend\Authentication\Authentication
     */
    private $authService;

    /**
     * Url view helper.
     * @var Zend\View\Helper\Url
     */
    private $urlHelper;

    /**
     * RBAC manager.
     * @var User\Service\RbacManager
     */
    private $rbacManager;

    /**
     * Constructs the service.
     */
    public function __construct($authService, $urlHelper, $rbacManager)
    {
        $this->authService = $authService;
        $this->urlHelper = $urlHelper;
        $this->rbacManager = $rbacManager;
    }

    /**
     * This method returns menu items depending on whether user has logged in or not.
     */
    public function getMenuItems()
    {
        $url = $this->urlHelper;
        $items = [];

        $items[] = [
            'id' => 'personal',
            'label' => 'PERSOONLIJK',
            'header' => 'header'
        ];

        $items[] = [
            'id' => 'home',
            'label' => 'Home',
            'link' => $url('home'),
            'icon' => 'fa fa-home'
        ];

        // Display "Login" menu item for not authorized user only. On the other hand,
        // display "Admin" and "Logout" menu items only for authorized users.
        if (!$this->authService->hasIdentity()) {
            $items[] = [
                'id' => 'login',
                'label' => 'Inloggen',
                'link' => $url('login'),
                'icon' => 'fa fa-user'
            ];
        } else {

            if ($this->rbacManager->isGranted(null, 'consumption.view')) {
                $items[] = [
                    'id' => 'consumption',
                    'label' => 'Consumptiehistorie',
                    'link' => $url('consumptie'),
                    'icon' => 'fa fa-beer'
                ];
            }

            if ($this->rbacManager->isGranted(null, 'balance.view')) {
                $items[] = [
                    'id' => 'balance',
                    'label' => 'Betalingen',
                    'link' => $url('betaling'),
                    'icon' => 'fa fa-bank'
                ];
            }

            $consumptionDropdownItems = [];
            $adminDropdownItems = [];

            if ($this->rbacManager->isGranted(null, 'payment.manage')) {
                $items[] = [
                    'id' => 'administrator',
                    'label' => 'ADMINISTRATIE',
                    'header' => 'header'
                ];
            }

            if ($this->rbacManager->isGranted(null, 'consumption.manage')) {
                $consumptionDropdownItems[] = [
                    'id' => 'consumptionOverview',
                    'label' => 'Overzicht',
                    'link' => $url('consumptie', ['action' => 'overview']),
                    'icon' => 'fa fa-beer'
                ];
            }

            if ($this->rbacManager->isGranted(null, 'consumption.manage')) {
                $consumptionDropdownItems[] = [
                    'id' => 'consumptionInput',
                    'label' => 'Bulkinvoer',
                    'link' => $url('consumptie', ['action' => 'insert']),
                    'icon' => 'fa fa-beer'
                ];
            }

            if ($this->rbacManager->isGranted(null, 'consumption.manage')) {
                $consumptionDropdownItems[] = [
                    'id' => 'specialInput',
                    'label' => 'Eten en overige',
                    'link' => $url('consumptie', ['action' => 'special']),
                    'icon' => 'fa fa-beer'
                ];
            }


            if (count($consumptionDropdownItems) != 0) {
                $items[] = [
                    'id' => 'consumption',
                    'label' => 'Consumptie',
                    'icon' => 'fa fa-beer',
                    'dropdown' => $consumptionDropdownItems
                ];
            }

            if ($this->rbacManager->isGranted(null, 'payment.manage')) {
                $items[] = [
                    'id' => 'payment',
                    'label' => 'Betalingen',
                    'link' => $url('betaling', ['action' => 'overview']),
                    'icon' => 'fa fa-bank'
                ];
            }

            if ($this->rbacManager->isGranted(null, 'product.manage')) {
                $items[] = [
                    'id' => 'product',
                    'label' => 'Producten',
                    'link' => $url('product'),
                    'icon' => 'fa fa-cubes'
                ];
            }

            if ($this->rbacManager->isGranted(null, 'priceCategory.manage')) {
                $items[] = [
                    'id' => 'priceCategory',
                    'label' => 'Prijscategorieën',
                    'link' => $url('prijscategorie'),
                    'icon' => 'fa fa-euro'
                ];
            }

            if ($this->rbacManager->isGranted(null, 'mail.manage')) {
                $items[] = [
                    'id' => 'mail',
                    'label' => 'E-mail',
                    'link' => $url('mail'),
                    'icon' => 'fa fa-envelope'
                ];
            }

            if ($this->rbacManager->isGranted(null, 'user.manage')) {
                $adminDropdownItems[] = [
                    'id' => 'gebruikers',
                    'label' => 'Gebruikersbeheer',
                    'link' => $url('gebruikers')
                ];
            }

            if ($this->rbacManager->isGranted(null, 'permission.manage')) {
                $adminDropdownItems[] = [
                    'id' => 'permissions',
                    'label' => 'Rechtenbeheer',
                    'link' => $url('permissions')
                ];
            }

            if ($this->rbacManager->isGranted(null, 'role.manage')) {
                $adminDropdownItems[] = [
                    'id' => 'roles',
                    'label' => 'Rollenbeheer',
                    'link' => $url('roles')
                ];
            }

            if (count($adminDropdownItems) != 0) {
                $items[] = [
                    'id' => 'admin',
                    'label' => 'Admin',
                    'icon' => 'fa fa-cogs',
                    'dropdown' => $adminDropdownItems
                ];
            }

            $items[] = [
                'id' => 'logout',
                'label' => $this->authService->getIdentity(),
                'float' => 'right',
                'dropdown' => [
                    [
                        'id' => 'settings',
                        'label' => 'Settings',
                        'link' => $url('application', ['action' => 'settings'])
                    ],
                    [
                        'id' => 'logout',
                        'label' => 'Sign out',
                        'link' => $url('logout')
                    ],
                ]
            ];
        }

        return $items;
    }
}


