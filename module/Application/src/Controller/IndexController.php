<?php

namespace Application\Controller;

use Consumption\Form\ConsumptionWidgetForm;
use Consumption\Service\ConsumptionService;
use Doctrine\ORM\EntityManager;
use Payment\Entity\Payment;
use Payment\Entity\TransactionHistory;
use User\Service\RbacManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Entity\User;

/**
 * This is the main controller class of the User Demo application. It contains
 * site-wide actions such as Home or About.
 */
class IndexController extends AbstractActionController
{
    /**
     * Entity manager.
     */
    private $entityManager;

    /**
     * Authentication service.
     * @var Zend\Authentication\AuthenticationService
     */
    private $authService;
    /**
     * @var ConsumptionService
     */
    private $consumptionService;
    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * Constructor. Its purpose is to inject dependencies into the controller.
     * @param EntityManager $entityManager
     * @param $authService
     * @param ConsumptionService $consumptionService
     * @param RbacManager $rbacManager
     */
    public function __construct(EntityManager $entityManager, $authService, ConsumptionService $consumptionService, RbacManager $rbacManager)
    {
        $this->entityManager = $entityManager;
        $this->authService = $authService;
        $this->consumptionService = $consumptionService;
        $this->rbacManager = $rbacManager;
    }

    /**
     * This is the default "index" action of the controller. It displays the
     * Home page.
     * @throws \Exception
     */
    public function indexAction()
    {
        $form = new ConsumptionWidgetForm($this->entityManager);

        $user = null;
        $balances = null;
        $balance = 0;

        if ($this->authService->hasIdentity()) {

            /** @var User $user */
            $user = $this->entityManager->getRepository(User::class)->findOneBy(array(
                'email' => $this->authService->getIdentity()
            ));

            /** @var TransactionHistory $balance */
            $balance = $user->getBalance();
        }

        if ($this->rbacManager->isGranted(null, 'payment.manage')) {
            $balances = $this->entityManager->getRepository(User::class)->findAllBalances()->getResult();
        }

        $transactions = $this->entityManager->getRepository(TransactionHistory::class)->findByUser($user->getId())->getResult();

        $transactionDates = [];
        $transactionAmounts = [];

        /** @var TransactionHistory $transaction */
        foreach ($transactions as $transaction) {
            $transactionDates[] = $transaction->getDate()->format('d-m-Y');
            $transactionAmounts[] = $transaction->getBalance();
        }

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);
            if (!$form->isValid()) {
                $this->flashMessenger()->addErrorMessage($form->getMessages());

                return $this->redirect()->toRoute('application', ['action' => 'index']);
            } else {
                $data = $form->getData();

                $this->consumptionService->addConsumption($data, $user);
                $this->flashMessenger()->addSuccessMessage('Consumptie is toegevoegd. Proost!');

                // Redirect to "view" page
                return $this->redirect()->toRoute('application', ['action' => 'index']);
            }
        }

        return new ViewModel([
            'form' => $form,
            'balance' => $balance,
            'transactionDates' => json_encode($transactionDates),
            'transactionAmounts' => json_encode($transactionAmounts),
            'balances' => $balances,
        ]);
    }

    /**
     * The "settings" action displays the info about currently logged in user.
     */
    public
    function settingsAction()
    {
        $id = $this->params()->fromRoute('id');

        if ($id != null) {
            $user = $this->entityManager->getRepository(User::class)
                ->find($id);
        } else {
            $user = $this->currentUser();
        }

        if ($user == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        if (!$this->access('profile.any.view') &&
            !$this->access('profile.own.view', ['user' => $user])) {
            return $this->redirect()->toRoute('not-authorized');
        }

        return new ViewModel([
            'user' => $user
        ]);
    }
}

