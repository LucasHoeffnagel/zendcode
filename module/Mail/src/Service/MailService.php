<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 19-6-19
 * Time: 12:39
 */

namespace Mail\Service;


use DateTime;
use Doctrine\ORM\EntityManager;
use Mail\Entity\MailHistory;
use User\Entity\User;
use Zend\Mail;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Renderer\PhpRenderer;

class MailService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * PHP template renderer.
     * @var Zend\View\Renderer\PhpRenderer
     */
    private $viewRenderer;

    /**
     * MailService constructor.
     * @param EntityManager $entityManager
     * @param PhpRenderer $viewRenderer
     */
    public function __construct(EntityManager $entityManager, PhpRenderer $viewRenderer)
    {
        $this->entityManager = $entityManager;
        $this->viewRenderer = $viewRenderer;
    }

    public function sendUpdate(User $sendBy)
    {
        $users = $this->entityManager->getRepository(User::class)->findAll();
        $date = new DateTime();

        $subject = "Update barsaldo " . $date->format('d-m-Y');

        foreach ($users as $user) {
            if($user->getId() === 7) {
                // Produce HTML of password reset email
                $bodyHtml = $this->viewRenderer->render(
                    'mail/email/balance-update',
                    [
                        'userBalance' => $user->getBalance(),
                        'date' => $date,
                        'userName' => $user->getFullName()
                    ]);

                $html = new MimePart($bodyHtml);
                $html->type = "text/html";

                $body = new MimeMessage();
                $body->addPart($html);

                $mail = new Mail\Message();
                $mail->setEncoding('UTF-8');
                $mail->setBody($body);
                $mail->setFrom('info@test.nl', 'Inventory');
                $mail->addTo($user->getEmail(), $user->getFullName());
                $mail->setSubject($subject);

                // Setup SMTP transport
                $transport = new SmtpTransport();
                $options = new SmtpOptions([
                    'name' => 'localhost.localdomain',
                    'host' => 'domain.nl',
                    'port' => 587,
                    'connection_class' => 'plain',
                    'connection_config' => [
                        'username' => 'user',
                        'password' => 'password',
                    ],
                ]);
                $transport->setOptions($options);

                $transport->send($mail);
            }
        }

        $mailHistory = new MailHistory();

        $mailHistory->setDate($date);
        $mailHistory->setSendBy($sendBy);

        $this->entityManager->persist($mailHistory);

        $this->entityManager->flush();
    }
}