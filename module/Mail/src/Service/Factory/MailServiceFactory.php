<?php

namespace Mail\Service\Factory;


use Interop\Container\ContainerInterface;
use Mail\Service\MailService;
use Zend\View\Renderer\PhpRenderer;

class MailServiceFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $viewRenderer = $container->get(PhpRenderer::class);

        // Instantiate the controller and inject dependencies
        return new MailService($entityManager, $viewRenderer);
    }
}