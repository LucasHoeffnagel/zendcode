<?php
namespace Mail\Repository;


use Doctrine\ORM\EntityRepository;
use Mail\Entity\MailHistory;

class MailRepository extends EntityRepository
{

    /**
     * Find last history record
     *
     * @return mixed
     */
    public function findLast(){

        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('m')
            ->from(MailHistory::class, 'm')
            ->setMaxResults(1)
            ->orderBy('m.date', 'DESC');

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}