<?php

namespace Mail\Controller;

use Mail\Entity\MailHistory;
use Mail\Service\MailService;
use User\Entity\User;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class MailController extends AbstractActionController
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;
    /**
     * @var MailService
     */
    private $mailService;
    /**
     * @var AuthenticationService
     */
    private $authService;

    /**
     * MailController constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param MailService $mailService
     * @param AuthenticationService $authService
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager, MailService $mailService, AuthenticationService $authService)
    {
        $this->entityManager = $entityManager;
        $this->mailService = $mailService;
        $this->authService = $authService;
    }

    public function indexAction()
    {

        $user = null;

        /** @var MailHistory $history */
        $history = $this->entityManager->getRepository(MailHistory::class)->findLast();
        if ($history != null) {
            $user = $history->getSendBy();
        }

        if ($user != null) {
            $username = $user->getFullName();
            $date = $history->getDate();
        } else {
            $username = '';
            $date = new \DateTime();
        }

        return new ViewModel([
            'userName' => $username,
            'date' => $date
        ]);
    }

    public function sendUpdateAction()
    {
        $user = null;

        if ($this->authService->hasIdentity()) {

            /** @var User $user */
            $user = $this->entityManager->getRepository(User::class)->findOneBy(array(
                'email' => $this->authService->getIdentity()
            ));
        }

        $this->mailService->sendUpdate($user);

        $this->flashMessenger()->addSuccessMessage('E-mail is verstuurd.');

        // Redirect to "view" page
        return $this->redirect()->toRoute('mail');
    }
}