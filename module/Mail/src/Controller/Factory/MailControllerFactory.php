<?php

namespace Mail\Controller\Factory;


use Consumption\Controller\ConsumptionController;
use Consumption\Service\ConsumptionService;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Mail\Controller\MailController;
use Mail\Service\MailService;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class MailControllerFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $authService = $container->get(AuthenticationService::class);
        $mailService = $container->get(MailService::class);

        return new MailController($entityManager, $mailService, $authService);
    }
}