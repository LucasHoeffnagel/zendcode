<?php
namespace Mail\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\Mail\Repository\MailRepository")
 * @ORM\Table(name="mail_history")
 */
class MailHistory
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="date")
     * @ORM\Column (type="datetime")
     */
    protected $date;

    /**
     * @ORM\ManyToOne(targetEntity="User\Entity\User", inversedBy="mail")
     * @ORM\JoinColumn(name="send_by", referencedColumnName="id")
     */
    protected $send_by;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getSendBy()
    {
        return $this->send_by;
    }

    /**
     * @param mixed $send_by
     */
    public function setSendBy($send_by): void
    {
        $this->send_by = $send_by;
    }


}