<?php

namespace Mail;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Mail\Controller\MailController;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'mail' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/mail[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => MailController::class,
                        'action' => 'index',
                    ],
                ],
            ],
        ],

    ],
    'controllers' => [
        'factories' => [
            Controller\MailController::class => Controller\Factory\MailControllerFactory::class
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\MailService::class => Service\Factory\MailServiceFactory::class
        ]
    ],
    // The 'access_filter' key is used by the User module to restrict or permit
    // access to certain controller actions for unauthorized visitors.
    'access_filter' => [
        'options' => [
            // The access filter can work in 'restrictive' (recommended) or 'permissive'
            // mode. In restrictive mode all controller actions must be explicitly listed 
            // under the 'access_filter' config key, and access is denied to any not listed 
            // action for not logged in users. In permissive mode, if an action is not listed 
            // under the 'access_filter' key, access to it is permitted to anyone (even for 
            // not logged in users. Restrictive mode is more secure and recommended to use.
            'mode' => 'restrictive'
        ],
        'controllers' => [
            Controller\MailController::class => [
                // Allow authorized users to visit "settings" action
                ['actions' => ['index', 'sendUpdate'], 'allow' => '+mail.manage']
            ]
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    // The following key allows to define custom styling for FlashMessenger view helper.
    'view_helper_config' => [
        'flashmessenger' => [
            'message_open_format' => '<p%s>',
            'message_close_string' => '</p>',
            'message_separator_string' => '<br>',
        ]
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],
];
