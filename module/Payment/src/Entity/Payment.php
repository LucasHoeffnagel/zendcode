<?php

namespace Payment\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a registered user.
 * @ORM\Entity(repositoryClass="\Payment\Repository\PaymentRepository")
 * @ORM\Table(name="payment")
 */
class Payment
{

    const TYPE_CASH = 1;
    const TYPE_BANK = 2;
    const TYPE_ADVANCE = 3;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="date_created")
     * @ORM\Column (type="datetime")
     */
    protected $dateCreated;

    /**
     * @ORM\Column(name="date_payed")
     * @ORM\Column (type="datetime")
     */
    protected $datePayed;

    /**
     * @ORM\ManyToOne(targetEntity="User\Entity\User", inversedBy="payment")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(name="amount")
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    protected $amount;

    /**
     * @ORM\Column(name="type", type="integer")
     */
    protected $type;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated): void
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDatePayed()
    {
        return $this->datePayed;
    }

    /**
     * @param mixed $datePayed
     */
    public function setDatePayed($datePayed): void
    {
        $this->datePayed = $datePayed;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }


}