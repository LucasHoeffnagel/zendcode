<?php

namespace Payment\Controller;

use DateTime;
use Doctrine\ORM\EntityManager;
use Payment\Entity\Payment;
use Payment\Form\PaymentForm;
use Payment\Service\BalanceService;
use Payment\Service\PaymentService;
use User\Entity\User;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class PaymentController extends AbstractActionController
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var BalanceService
     */
    private $balanceService;
    /**
     * @var PaymentService
     */
    private $paymentService;
    /**
     * @var AuthenticationService
     */
    private $authService;

    /**
     * PaymentController constructor.
     * @param EntityManager $entityManager
     * @param BalanceService $balanceService
     * @param PaymentService $paymentService
     * @param AuthenticationService $authService
     */
    public function __construct(EntityManager $entityManager, BalanceService $balanceService, PaymentService $paymentService, AuthenticationService $authService)
    {
        $this->entityManager = $entityManager;
        $this->balanceService = $balanceService;
        $this->paymentService = $paymentService;
        $this->authService = $authService;
    }

    public function indexAction()
    {
        $user = null;

        if ($this->authService->hasIdentity()) {

            /** @var User $user */
            $user = $this->entityManager->getRepository(User::class)->findOneBy(array(
                'email' => $this->authService->getIdentity()
            ));
        }

        /** @var Payment $payments */
        $payments = $this->entityManager->getRepository(Payment::class)->findByUser($user->getId())->getResult();

        return new ViewModel([
            'payments' => $payments
        ]);
    }

    public function overviewAction()
    {
        $payments = $this->entityManager->getRepository(Payment::class)->findAll();

        $form = new PaymentForm($this->entityManager);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);
            if (!$form->isValid()) {
                $this->flashMessenger()->addErrorMessage($form->getMessages());

                return $this->redirect()->toRoute('betaling');
            } else {
                $data = $form->getData();
                $date = new DateTime;

                $user = $this->entityManager->getRepository(User::class)->find($data['user']);

                $payment = new Payment();
                $amount = $data['amount'];

                $payment->setDateCreated($date);
                $payment->setAmount($amount);
                $payment->setUser($user);
                $payment->setType($data['type']);
                $paymentDate = new DateTime($data['paymentDate']);
                $payment->setDatePayed($paymentDate);

                /** Update the balance of the user and add a new history item */
                $this->balanceService->updateBalance($amount, $user, $date);

                $this->entityManager->persist($payment);
                $this->entityManager->flush();

                $this->flashMessenger()->addSuccessMessage('Betaling is verwerkt.');

                // Redirect to "view" page
                return $this->redirect()->toRoute('betaling', ['action' => 'overview']);
            }
        }

        return new ViewModel([
            'form' => $form,
            'payments' => $payments
        ]);
    }

    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);

        $this->paymentService->deletePayment($id);

        $this->flashMessenger()->addSuccessMessage('Betaling is verwijderd.');

        return $this->redirect()->toRoute('betaling', ['action' => 'overview']);
    }
}