<?php

namespace Payment\Controller\Factory;

use Payment\Controller\PaymentController;
use Payment\Service\BalanceService;
use Payment\Service\PaymentService;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class PaymentControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $balanceService = $container->get(BalanceService::class);
        $paymentService = $container->get(PaymentService::class);
        $authService = $container->get(AuthenticationService::class);

        return new PaymentController($entityManager, $balanceService, $paymentService, $authService);
    }
}