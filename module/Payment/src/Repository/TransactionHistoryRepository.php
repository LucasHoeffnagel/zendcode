<?php

namespace Payment\Repository;

use Doctrine\ORM\EntityRepository;
use Payment\Entity\TransactionHistory;

class TransactionHistoryRepository extends EntityRepository
{
    /**
     * Retrieve all transactions for the logged in user
     *
     * @param $id
     * @return \Doctrine\ORM\Query
     */
    public function findByUser($id)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();
        $expr = $queryBuilder->expr();

        $queryBuilder->select('t')
            ->from(TransactionHistory::class, 't')
            ->where($expr->eq('t.user', $id))
            ->orderBy('t.date', 'ASC');

        return $queryBuilder->getQuery();
    }
}