<?php

namespace Payment\Repository;


use Doctrine\ORM\EntityRepository;
use Payment\Entity\Payment;

class PaymentRepository extends EntityRepository
{
    public function findByUser($id)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();
        $expr = $queryBuilder->expr();

        $queryBuilder->select('p')
            ->from(Payment::class, 'p')
            ->where($expr->eq('p.user', $id))
            ->orderBy('p.datePayed', 'DESC');

        return $queryBuilder->getQuery();
    }
}