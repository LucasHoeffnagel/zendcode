<?php

namespace Payment\Form;


use Doctrine\ORM\EntityManager;
use Payment\Entity\Payment;
use User\Entity\User;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Date;
use Zend\Form\Element\Select;
use Zend\Form\Form;
use DateTime;

class PaymentForm extends Form
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        // Define form name
        parent::__construct('payment-form');

        $this->entityManager = $entityManager;

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        // Add form elements
        $this->addElements();
    }

    private function addElements()
    {
        $this->add([
            'type' => Csrf::class,
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600,
                ],
            ],
        ]);

        $this->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'user',
            'attributes' => [
                'id' => 'user',
                'placeholder' => 'Naam',
                'class' => 'form-control input-sm'
            ],
            'options' => [
                'object_manager' => $this->entityManager,
                'target_class' => User::class,
                'label_generator' => function ($targetEntity) {
                    return $targetEntity->getFullName();
                },
                'label' => 'Gebruiker',
                'find_method' => [
                    'name' => 'findBy',
                    'params' => [
                        'criteria' => [],
                        'orderBy' => ['fullName' => 'asc'],
                    ]
                ]
            ]
        ]);

        $this->add([
            'type' => 'number',
            'name' => 'amount',
            'attributes' => [
                'id' => 'amount',
                'placeholder' => 'Bedrag',
                'class' => 'form-control input-sm',
                'step' => 0.01,
                'required' => true,
            ],
            'options' => [
                'label' => 'Bedrag'
            ]
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'paymentDate',
            'attributes' => [
                'placeholder' => 'Datum betaling',
                'id' => 'paymentDate',
                'class' => 'form-control input-sm',
                'required' => true,
                'autocomplete' => 'off'
            ],
            'options' => [
                'label' => 'Datum betaling',
            ]
        ]);

        $this->add([
            'type' => Select::class,
            'name' => 'type',
            'attributes' => [
                'id' => 'type',
                'placeholder' => 'Soort betaling',
                'class' => 'form-control input-sm',
            ],
            'options' => [
                'value_options' => [
                    Payment::TYPE_CASH => 'Contant',
                    Payment::TYPE_BANK => 'Overschrijving',
                    Payment::TYPE_ADVANCE => 'Voorschot'
                ],
                'label' => 'Soort betaling'
            ]
        ]);


        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Opslaan',
                'class' => 'btn btn-primary pull-right'
            ],
        ]);
    }
}