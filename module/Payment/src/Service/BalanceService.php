<?php

namespace Payment\Service;


use Doctrine\ORM\EntityManager;
use Payment\Entity\TransactionHistory;
use User\Entity\User;

class BalanceService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * balanceService constructor.
     * @param EntityManager $entityManager
     *
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    /**
     * Get the current balance of a user and update it
     *
     * @param $amount
     * @param $user
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     */
    public function updateBalance($amount, $user, $date)
    {

        $currentBalance = $user->getBalance();

        $newBalance = $currentBalance + $amount;

        $transactionHistory = new TransactionHistory();

        $transactionHistory->setUser($user);
        $transactionHistory->setAmount($amount);
        $transactionHistory->setDate($date);
        $transactionHistory->setBalance($newBalance);

        $user->setBalance($newBalance);

        $this->entityManager->persist($transactionHistory);
        $this->entityManager->merge($user);

        return true;
    }
}