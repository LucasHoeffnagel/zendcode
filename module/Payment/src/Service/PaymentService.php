<?php

namespace Payment\Service;


use DateTime;
use Doctrine\ORM\EntityManager;
use Payment\Entity\Payment;
use User\Entity\User;

class PaymentService
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var BalanceService
     */
    private $balanceService;

    /**
     * PaymentService constructor.
     * @param EntityManager $entityManager
     * @param BalanceService $balanceService
     */
    public function __construct(EntityManager $entityManager, BalanceService $balanceService)
    {
        $this->entityManager = $entityManager;
        $this->balanceService = $balanceService;
    }

    /**
     * Delete a payment
     *
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deletePayment($id)
    {
        $date = new DateTime;

        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)->find($id);

        /** @var User $user */
        $user = $payment->getUser();
        $amount = $payment->getAmount();

        $this->balanceService->updateBalance($amount * -1, $user, $date);
        $this->entityManager->remove($payment);

        $this->entityManager->flush();
    }
}