<?php

namespace Payment\Service\Factory;


use Interop\Container\ContainerInterface;
use Payment\Service\BalanceService;
use Payment\Service\PaymentService;

class PaymentServiceFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $balanceService = $container->get(BalanceService::class);

        // Instantiate the controller and inject dependencies
        return new PaymentService($entityManager, $balanceService);
    }
}