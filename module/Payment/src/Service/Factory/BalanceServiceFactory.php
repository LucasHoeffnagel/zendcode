<?php

namespace Payment\Service\Factory;


use Interop\Container\ContainerInterface;
use Payment\Service\BalanceService;

class BalanceServiceFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        // Instantiate the controller and inject dependencies
        return new BalanceService($entityManager);
    }
}