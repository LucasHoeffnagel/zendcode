<?php

namespace Consumption\Service;

use Consumption\Entity\Consumption;
use Consumption\Entity\SpecialConsumption;
use DateTime;
use Doctrine\ORM\EntityManager;
use Payment\Service\BalanceService;
use Product\Entity\PriceCategory;
use Product\Entity\Product;
use User\Entity\User;

class ConsumptionService
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var BalanceService
     */
    private $balanceService;

    /**
     * ConsumptionService constructor.
     * @param EntityManager $entityManager
     * @param BalanceService $balanceService
     */
    public function __construct(EntityManager $entityManager, BalanceService $balanceService)
    {
        $this->entityManager = $entityManager;
        $this->balanceService = $balanceService;
    }

    /**
     * Add a new consumption for the logged in user
     *
     * @param $data
     * @param $user
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addConsumption($data, $user)
    {
        $date = new DateTime;
        $consumption = new Consumption();
        $amount = $data['amount'];

        /** @var Product $product */
        $product = $this->entityManager->getRepository(Product::class)->find($data['product']);

        /** @var PriceCategory $priceCategory */
        $priceCategory = $product->getPriceCategory();
        $totalPrice = $product->getPriceCategory()->getPrice() * $data['amount'];

        $consumption->setAmount($amount);
        $consumption->setDate($date);
        $consumption->setProduct($product);
        $consumption->setUser($user);
        $consumption->setPriceCategory($priceCategory);
        $consumption->setTotalPrice($totalPrice);
        $consumption->setActive(true);

        $this->balanceService->updateBalance($totalPrice * -1, $user, $date);
        $product->setStock($product->getStock() - 1);

        $this->entityManager->merge($product);
        $this->entityManager->persist($consumption);

        $this->entityManager->flush();

        return true;
    }

    /**
     * Set a consumption to inactive and update a users balance
     *
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteConsumption($id)
    {
        $date = new DateTime;

        /** @var Consumption $consumption */
        $consumption = $this->entityManager->getRepository(Consumption::class)->find($id);
        $user = $consumption->getUser();

        $consumption->setActive(false);
        $this->balanceService->updateBalance($consumption->getTotalPrice(), $user, $date);

        $this->entityManager->merge($consumption);

        $this->entityManager->flush();
    }

    /**
     * Set a consumption active and update a users balance
     *
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setConsumptionActive($id)
    {
        $date = new DateTime;

        /** @var Consumption $consumption */
        $consumption = $this->entityManager->getRepository(Consumption::class)->find($id);
        $user = $consumption->getUser();

        $consumption->setActive(true);
        $this->balanceService->updateBalance($consumption->getTotalPrice() * -1, $user, $date);

        $this->entityManager->merge($consumption);

        $this->entityManager->flush();
    }

    /**
     * Create a special consumption for each user in the array
     *
     * @param $data
     * @param User $user
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addSpecialConsumption($data, $users)
    {
        foreach ($users as $user) {
            $this->addSpecialConsumptionForUser($data, $user);
        }

        return true;
    }

    /**
     * @param $data
     * @param $userId
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function addSpecialConsumptionForUser($data, $userId)
    {
        $date = new DateTime;

        $user = $this->entityManager->getRepository(User::class)->find($userId);

        /** @var SpecialConsumption $specialConsumption */
        $specialConsumption = new SpecialConsumption();

        $specialConsumption->setUser($user);
        $specialConsumption->setDateCreated($date);
        $specialConsumption->setDescription($data['description']);
        $specialConsumption->setAmount($data['amount']);
        $specialConsumption->setDateConsumption(new DateTime($data['dateConsumption']));

        $totalPrice = $data['price'] * $data['amount'];
        $specialConsumption->setTotalPrice($totalPrice);
        $specialConsumption->setActive(true);

        $this->balanceService->updateBalance($totalPrice * -1, $user, $date);

        $this->entityManager->persist($specialConsumption);

        $this->entityManager->flush();

        return true;
    }

    /**
     * Set a special consumption inactive
     *
     * @param $id
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteSpecialConsumption($id)
    {
        $date = new DateTime;

        /** @var SpecialConsumption $specialConsumption */
        $specialConsumption = $this->entityManager->getRepository(SpecialConsumption::class)->find($id);

        $user = $specialConsumption->getUser();

        $specialConsumption->setActive(false);
        $this->balanceService->updateBalance($specialConsumption->getTotalPrice(), $user, $date);

        $this->entityManager->merge($specialConsumption);

        $this->entityManager->flush();

        return true;
    }

    /**
     * Set a special consumption active
     *
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setSpecialActive($id)
    {
        $date = new DateTime;

        /** @var SpecialConsumption $specialConsumption */
        $specialConsumption = $this->entityManager->getRepository(SpecialConsumption::class)->find($id);
        $user = $specialConsumption->getUser();

        $specialConsumption->setActive(true);
        $this->balanceService->updateBalance($specialConsumption->getTotalPrice() * -1, $user, $date);

        $this->entityManager->merge($specialConsumption);

        $this->entityManager->flush();
    }

    public function addBulkConsumption($data) {

        $user = $this->entityManager->getRepository(User::class)->find($data['user']);
        $priceCategory = $this->entityManager->getRepository(PriceCategory::class)->find($data['priceCategory']);
        $price = $data['amount'] * $priceCategory->getPrice();
        $date = new DateTime($data['date']);
        $date->setTime(0, 0);

        $consumption = new Consumption();

        $consumption->setActive(1);
        $consumption->setUser($user);
        $consumption->setPriceCategory($priceCategory);
        $consumption->setTotalPrice($price);
        $consumption->setDate($date);
        $consumption->setAmount($data['amount']);

        $this->balanceService->updateBalance($price * -1, $user, $date);

        $this->entityManager->persist($consumption);

        $this->entityManager->flush();
    }
}