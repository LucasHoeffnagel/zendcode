<?php
namespace Consumption\Service\Factory;


use Consumption\Service\ConsumptionService;
use Interop\Container\ContainerInterface;
use Payment\Service\BalanceService;

class ConsumptionServiceFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $balanceService = $container->get(BalanceService::class);

        // Instantiate the controller and inject dependencies
        return new ConsumptionService($entityManager, $balanceService);
    }
}