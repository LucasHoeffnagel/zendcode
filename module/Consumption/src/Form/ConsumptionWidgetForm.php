<?php

namespace Consumption\Form;


use Doctrine\ORM\EntityManager;
use Product\Entity\Product;
use Zend\Form\Element\Csrf;
use Zend\Form\Form;

class ConsumptionWidgetForm extends Form
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        // Define form name
        parent::__construct('consumption-form');

        $this->entityManager = $entityManager;

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        // Add form elements
        $this->addElements();
    }

    private function addElements()
    {
        $this->add([
            'type' => Csrf::class,
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600,
                ],
            ],
        ]);

        $this->add([
            'type' => 'number',
            'name' => 'amount',
            'attributes' => [
                'id' => 'amount',
                'placeholder' => 'Aantal',
                'class' => 'form-control input-sm',
                'min' => 1,
                'max' => 1000,
                'value' => 1,
                'required' => true
            ]
        ]);

        $this->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'product',
            'attributes' => [
                'id' => 'product',
                'placeholder' => 'Product',
                'class' => 'form-control input-sm'
            ],
            'options' => [
                'object_manager' => $this->entityManager,
                'target_class' => Product::class,
                'label_generator' => function ($targetEntity) {
                /** @var Product $targetEntity */
                    return $targetEntity->getName(). ' - €' . $targetEntity->getPriceCategory()->getPrice();
                },
                'find_method' => [
                    'name' => 'findBy',
                    'params' => [
                        'criteria' => [],
                        'orderBy' => ['name' => 'asc'],
                    ]
                ]
            ]
        ]);

        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Bestellen',
                'class' => 'btn btn-primary pull-right'
            ],
        ]);
    }
}