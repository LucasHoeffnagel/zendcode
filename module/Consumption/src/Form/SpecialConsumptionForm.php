<?php
namespace Consumption\Form;


use Doctrine\ORM\EntityManager;
use User\Entity\User;
use Zend\Form\Element\Csrf;
use Zend\Form\Form;

class SpecialConsumptionForm extends form
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        // Define form name
        parent::__construct('product-form');

        $this->entityManager = $entityManager;

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        // Add form elements
        $this->addElements();
    }

    private function addElements()
    {
        $this->add([
            'type' => Csrf::class,
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600,
                ],
            ],
        ]);

        $this->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'user',
            'attributes' => [
                'id' => 'user',
                'placeholder' => 'Naam',
                'class' => 'form-control input-sm',
                'multiple' => 'multiple',
            ],
            'options' => [
                'object_manager' => $this->entityManager,
                'target_class' => User::class,
                'property' => 'fullName',
                'label' => 'Gebruiker',
            ]
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'dateConsumption',
            'attributes' => [
                'placeholder' => 'Datum',
                'id' => 'dateConsumption',
                'class' => 'form-control input-sm',
                'required' => true,
                'autocomplete' => 'off'
            ],
            'options' => [
                'label' => 'Datum',
            ]
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'description',
            'attributes' => [
                'placeholder' => 'Beschrijving',
                'id' => 'description',
                'class' => 'form-control input-sm',
                'required' => true,
                'autocomplete' => 'off'
            ],
            'options' => [
                'label' => 'Beschrijving',
            ]
        ]);

        $this->add([
            'type' => 'number',
            'name' => 'price',
            'attributes' => [
                'id' => 'price',
                'placeholder' => 'Bedrag',
                'class' => 'form-control input-sm',
                'step' => 0.01,
                'required' => true,
            ],
            'options' => [
                'label' => 'Bedrag'
            ]
        ]);

        $this->add([
            'type' => 'number',
            'name' => 'amount',
            'attributes' => [
                'id' => 'amount',
                'placeholder' => 'Aantal',
                'class' => 'form-control input-sm',
                'step' => 1,
                'min' => 1,
                'required' => true,
            ],
            'options' => [
                'label' => 'Aantal'
            ]
        ]);


        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Opslaan',
                'id' => 'submit',
                'class' => 'btn btn-primary pull-right'
            ],
        ]);
    }
}