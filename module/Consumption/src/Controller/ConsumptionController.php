<?php

namespace Consumption\Controller;


use Consumption\Entity\Consumption;
use Consumption\Entity\SpecialConsumption;
use Consumption\Form\ConsumptionForm;
use Consumption\Form\SpecialConsumptionForm;
use Consumption\Service\ConsumptionService;
use User\Entity\User;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ConsumptionController extends AbstractActionController
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;
    /**
     * @var AuthenticationService
     */
    private $authService;
    /**
     * @var ConsumptionService
     */
    private $consumptionService;

    /**
     * ConsumptionController constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param AuthenticationService $authService
     * @param ConsumptionService $consumptionService
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager, AuthenticationService $authService, ConsumptionService $consumptionService)
    {
        $this->entityManager = $entityManager;
        $this->authService = $authService;
        $this->consumptionService = $consumptionService;
    }

    public function indexAction()
    {
        $user = null;

        if ($this->authService->hasIdentity()) {
            /** @var User $user */
            $user = $this->entityManager->getRepository(User::class)->findOneBy(array(
                'email' => $this->authService->getIdentity()
            ));
        }

        $consumptions = $this->entityManager->getRepository(Consumption::class)
            ->findByUser($user->getId())->getResult();

        $specialConsumptions = $this->entityManager->getRepository(SpecialConsumption::class)
            ->findByUser($user->getId())->getResult();

        return new ViewModel([
            'consumptions' => $consumptions,
            'specialConsumptions' => $specialConsumptions
        ]);
    }

    public function insertAction()
    {
        $form = new ConsumptionForm($this->entityManager);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);
            if (!$form->isValid()) {
                $this->flashMessenger()->addErrorMessage($form->getMessages());
                return $this->redirect()->toRoute('consumptie', ['action' => 'insert']);
            } else {
                $this->consumptionService->addBulkConsumption($data);

                $this->flashMessenger()->addSuccessMessage('Consumpties zijn toegevoegd.');

                return $this->redirect()->toRoute('consumptie', ['action' => 'insert']);
            }

        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    /**
     * Set a consumption to inactive
     *
     * @return \Zend\Http\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);

        $this->consumptionService->deleteConsumption($id);

        $this->flashMessenger()->addSuccessMessage('Consumptie is verwijderd.');

        // Redirect to "view" page
        return $this->redirect()->toRoute('consumptie');

    }

    /**
     * Get all active and inactive consumptions
     *
     * @return ViewModel
     */
    public function overviewAction()
    {
        $consumptions = $this->entityManager->getRepository(Consumption::class)->findAll();

        return new ViewModel([
            'consumptions' => $consumptions
        ]);
    }

    public function adminDeleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);

        $this->consumptionService->deleteConsumption($id);

        $this->flashMessenger()->addSuccessMessage('Consumptie is op inactief gezet.');

        // Redirect to "view" page
        return $this->redirect()->toRoute('consumptie', ['action' => 'overview']);
    }

    public function setActiveAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);

        $this->consumptionService->setConsumptionActive($id);

        $this->flashMessenger()->addSuccessMessage('Consumptie is op actief gezet.');

        // Redirect to "view" page
        return $this->redirect()->toRoute('consumptie', ['action' => 'overview']);
    }

    public function specialAction()
    {
        $specialConsumptions = $this->entityManager->getRepository(SpecialConsumption::class)->findAll();

        $form = new SpecialConsumptionForm($this->entityManager);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);

            if (!$form->isValid()) {
                $this->flashMessenger()->addErrorMessage($form->getMessages());

                return $this->redirect()->toRoute('consumptie', ['action' => 'special']);
            } else {
                $data = $form->getData();

                $this->consumptionService->addSpecialConsumption($data, $data['user']);

                $this->flashMessenger()->addSuccessMessage('Consumptie is ingevoerd.');

                // Redirect to "view" page
                return $this->redirect()->toRoute('consumptie', ['action' => 'special']);
            }
        }

        return new ViewModel([
            'form' => $form,
            'specialConsumptions' => $specialConsumptions
        ]);
    }

    public function deleteSpecialAction() {
        $id = (int)$this->params()->fromRoute('id', -1);

        $this->consumptionService->deleteSpecialConsumption($id);

        $this->flashMessenger()->addSuccessMessage('Speciale consumptie is op inactief gezet.');

        // Redirect to "view" page
        return $this->redirect()->toRoute('consumptie', ['action' => 'special']);
    }

    public function setSpecialActiveAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);

        $this->consumptionService->setSpecialActive($id);

        $this->flashMessenger()->addSuccessMessage('Consumptie is op actief gezet.');

        // Redirect to "view" page
        return $this->redirect()->toRoute('consumptie', ['action' => 'special']);
    }
}