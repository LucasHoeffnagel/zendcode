<?php

namespace Consumption\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\Consumption\Repository\ConsumptionRepository")
 * @ORM\Table(name="consumption")
 */
class Consumption
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="date")
     * @ORM\Column (type="datetime")
     */
    protected $date;

    /**
     * @ORM\ManyToOne(targetEntity="User\Entity\User", inversedBy="consumption")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    protected $user;


    /**
     * @ORM\ManyToOne(targetEntity="Product\Entity\PriceCategory", inversedBy="consumption")
     * @ORM\JoinColumn(name="price_category", referencedColumnName="id")
     */
    protected $priceCategory;

    /**
     * @ORM\ManyToOne(targetEntity="Product\Entity\Product", inversedBy="consumption")
     * @ORM\JoinColumn(name="product", referencedColumnName="id")
     */
    protected $product;

    /**
     * @ORM\Column(name="amount", type="integer")
     */
    protected $amount;

    /**
     * @ORM\Column(name="total_price")
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    protected $total_price;

    /**
     * @ORM\Column(name="active")
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getPriceCategory()
    {
        return $this->priceCategory;
    }

    /**
     * @param mixed $priceCategory
     */
    public function setPriceCategory($priceCategory): void
    {
        $this->priceCategory = $priceCategory;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->total_price;
    }

    /**
     * @param mixed $total_price
     */
    public function setTotalPrice($total_price): void
    {
        $this->total_price = $total_price;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product): void
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }
}