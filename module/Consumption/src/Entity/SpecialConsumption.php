<?php

namespace Consumption\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a registered user.
 * @ORM\Entity(repositoryClass="\Consumption\Repository\SpecialConsumptionRepository")
 * @ORM\Table(name="special_consumption")
 */
class SpecialConsumption
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="date_created")
     * @ORM\Column (type="datetime")
     */
    protected $dateCreated;

    /**
     * @ORM\Column(name="date_consumption")
     * @ORM\Column (type="datetime")
     */
    protected $dateConsumption;


    /**
     * @ORM\ManyToOne(targetEntity="User\Entity\User", inversedBy="specialConsumption")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(name="description")
     */
    protected $description;

    /**
     * @ORM\Column(name="amount", type="integer")
     */
    protected $amount;

    /**
     * @ORM\Column(name="total_price")
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    protected $total_price;

    /**
     * @ORM\Column(name="active")
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->total_price;
    }

    /**
     * @param mixed $total_price
     */
    public function setTotalPrice($total_price): void
    {
        $this->total_price = $total_price;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated): void
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateConsumption()
    {
        return $this->dateConsumption;
    }

    /**
     * @param mixed $dateConsumption
     */
    public function setDateConsumption($dateConsumption): void
    {
        $this->dateConsumption = $dateConsumption;
    }

}

