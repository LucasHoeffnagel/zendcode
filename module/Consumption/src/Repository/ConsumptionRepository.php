<?php

namespace Consumption\Repository;


use Consumption\Entity\Consumption;
use Doctrine\ORM\EntityRepository;

class ConsumptionRepository extends EntityRepository
{
    /**
     * Retrieve all consumptions for the logged in user
     *
     * @param $id
     * @return \Doctrine\ORM\Query
     */
    public function findByUser($id)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();
        $expr = $queryBuilder->expr();

        $queryBuilder->select('u')
            ->from(Consumption::class, 'u')
            ->where($expr->eq('u.user', $id))
            ->orderBy('u.date', 'DESC');

        return $queryBuilder->getQuery();
    }
}