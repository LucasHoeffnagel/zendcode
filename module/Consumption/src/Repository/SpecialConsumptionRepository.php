<?php

namespace Consumption\Repository;


use Consumption\Entity\SpecialConsumption;
use Doctrine\ORM\EntityRepository;

class SpecialConsumptionRepository extends EntityRepository
{
    /**
     * Retrieve all specialConsumptions for the logged in user
     *
     * @param $id
     * @return \Doctrine\ORM\Query
     */
    public function findByUser($id)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();
        $expr = $queryBuilder->expr();

        $queryBuilder->select('s')
            ->from(SpecialConsumption::class, 's')
            ->where($expr->eq('s.user', $id))
            ->orderBy('s.dateConsumption', 'DESC');

        return $queryBuilder->getQuery();
    }
}